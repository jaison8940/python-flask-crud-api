from database import db
from models import users
import json
import dbcreate


def convert_Users(users):
    res = {}
    index = 0
    for user in users:
        temp = {}
        temp = {
            'id': user.id,
            'name': user.name,
            'email': user.email,
            'contactno': user.contactno}
        res[index] =  temp
        index += 1
    return res


def get_Users_Data(id):
    try:
        if id:
            result = users.query.filter_by(id = id)
        else:
            result = users.query.all()
        return result
    except Exception as error:
        res = {"error occurred" : str(error.__class__)}
        return res
    


def get_Users(id=None):
    try:       
        user_results = get_Users_Data(id)        
        res = convert_Users(user_results)
        return res
    except Exception as error:
        res = {"error occurred" : str(error.__class__)}
        return res


def post_Users_Data(data):
    try:
        user_array = []
        for user in data:
            temp = users(name=user['name'],email=user['email'],contactno=user['contactno'])
            user_array.append(temp)
            db.session.add(temp)
        db.session.commit()
        res = convert_Users(user_array)
        return json.dumps(res)
    except Exception as error:
        res = {"error occurred":str(error.__class__)}
        return json.dumps(res)
      

def post_Users(data):
    try:
        
        data = json.loads(data)
        print(data)
        if isinstance(data,dict):
            data = [data]        
        response = post_Users_Data(data)
        return response
    except Exception as error:
        res = {"error occurred":str(error.__class__)}
        return json.dumps(res)


def put_Users_Data(id,data):
    user_array = []        
    updateUser = users.query.get(id)
    user_array.append(updateUser)
    updateUser.name = data['name']
    updateUser.email = data['email']
    updateUser.contactno = data['contactno']
    db.session.commit()
    res = convert_Users(user_array)
    return res


def put_Users(id,data):
    try:
        data = json.loads(data)
        response  = put_Users_Data(id,data)
        return json.dumps(response)
    except Exception as error:
        res = {"error occurred" : str(error.__class__)}
        return json.dumps(res)


def partial_Update_Users_Data(id, data):
    try:
        updateUser = users.query.get(id)
        if 'name' in data and data['name'] != updateUser.name:
            updateUser.name = data['name']
        if 'email' in data and data['email'] != updateUser.email:
            updateUser.email = data['email']
        if 'contactno' in data and data['contactno'] != updateUser.contactno:
            updateUser.contactno = data['contactno']
        res = convert_Users([updateUser])
        return res
    except Exception as error:
        res = {"error occurred" : str(error.__class__)}
        print(res)
        return res

def partial_Update_Users(id, data):
    try:
        print("partial update")
        data = json.loads(data)
        response  = partial_Update_Users_Data(id,data)
        return json.dumps(response)
    except Exception as error:
        res = {"error occurred" : str(error.__class__)}
        
        return json.dumps(res)


def delete_User_Data(id):
    try:
        db.session.delete(users.query.get(id))
        db.session.commit()
        return "Deleted Successfully"
    except Exception as error:
        res = {"error occurred" : str(error.__class__)}
        return res


def delete_User(id = None):
    try:
        if not id:
            raise Exception("id is required")
        response = delete_User_Data(id)
        return json.dumps(response)       
    except Exception as error:
        res = {"error occurred" : str(error.__class__)}
        return json.dumps(res)
    


    