from flaskapp import app,request
from service import get_Users, post_Users, put_Users, delete_User, partial_Update_Users


@app.route('/users', methods=['GET'])
@app.route('/users/<id>', methods=['GET'])
def read_users(id=None):
    return get_Users(id)


@app.route('/users', methods=['POST'])
def insert_users():
    return post_Users(request.data)


@app.route('/users/<id>', methods=['PUT', 'POST'])
def update_users(id):
    if request.method == 'PUT':
        return put_Users(id, request.data)
    elif request.method == 'POST':
        return partial_Update_Users(id,request.data)


@app.route('/users/<id>', methods=['DELETE'])
def delete_user(id):
    return delete_User(id)
    
    
if __name__ == '__main__':
    # app.run(host="0.0.0.0", port=5000,debug = True) docker
    app.run(debug = True)